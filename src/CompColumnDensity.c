/*
 * Computes column density along the z grid direction of the first fluid
 * Many optimizations are possible, but if calling only every DT might not be worth it.
 * Colin McNally QMUL colin@columncnally.ca
 */

#include "fargo3d.h"

void ComputeColumnDensity() {

  real* dens = Density->field_cpu;
  real* coldensu = ColumnDensityU->field_cpu;
  real* coldensd = ColumnDensityD->field_cpu;
#ifdef TAUCOLUMN
  real* energy  = Energy->field_cpu;
  real* taucolu = TauColumnU->field_cpu;
  real* taucold = TauColumnD->field_cpu;
#endif
  int pitch  = Pitch_cpu;
  int stride = Stride_cpu;
  int size_x = Nx+2*NGHX;
  int size_y = Ny+2*NGHY;
  int size_z = Nz+2*NGHZ;

  int i;
  int j;
  int k;
  int ll, llzm, llzp;
  int stage, stages, newstage;
  long int commflags;

  // Only acts on the first fluid
  SelectFluid(0); 
  
  stages =  Gridd.NK;

//  if (Gridd.J+Gridd.K==0) printf("CompColumnDensity %i %i %i %i stages %i\n",Gridd.J,Gridd.K,Gridd.NJ,Gridd.NK,stages);

  i = j = k = 0;
#ifdef Z
  for (k=0; k<size_z; k++) {
#endif
#ifdef Y
    for (j=0; j<size_y; j++) {
#endif
#ifdef X
      for (i=0; i<size_x; i++ ) {
#endif
//<#>
        ll = l;
        coldensu[ll] = 0.0;
        coldensd[ll] = 0.0;
#ifdef TAUCOLUMN
        taucolu[ll] = 0.0;
        taucold[ll] = 0.0;
#endif
//<\#>
#ifdef X
      }
#endif
#ifdef Y
    }
#endif
#ifdef Z
  }
#endif

  for (stage=0; stage < stages; stage++){ 
    i = j = k = 0;
#ifdef Z
    for (k=NGHZ; k<size_z-NGHZ; k++) {
#endif
#ifdef Y
      for (j=0; j<size_y; j++) {
#endif
#ifdef X
        for (i=0; i<size_x; i++ ) {
#endif
//<#>
          ll = l;
          llzp = lzp;
          llzm = lzm;
          if (Gridd.K==stage || Gridd.NK-Gridd.K-1==stage){
              coldensu[ll] = dens[ll]*zone_size_z(j,k) + coldensu[llzm];
#ifdef TAUCOLUMN
              // To be user-defined, might want to use the gas energy field
              taucolu[ll] = zone_size_z(j,k) + taucolu[llzm];
#endif
          }
//<\#>
#ifdef X
        }
#endif
#ifdef Y
      }
#endif
#ifdef Z
    }
#endif
    i = j = k = 0;
#ifdef Z
    for (k=size_z-NGHZ-1; k>=NGHZ; k--) {
#endif
#ifdef Y
      for (j=0; j<size_y; j++) {
#endif
#ifdef X
        for (i=0; i<size_x; i++ ) {
#endif
//<#>
          ll = l;
          llzp = lzp;
          llzm = lzm;
          if (Gridd.K==stage || Gridd.NK-Gridd.K-1==stage){
              coldensd[ll] = dens[ll]*zone_size_z(j,k) + coldensd[llzp];
#ifdef TAUCOLUMN
              taucold[ll] = zone_size_z(j,k) + taucold[llzp];
#endif
          }
//<\#>
#ifdef X
        }
#endif
#ifdef Y
      }
#endif
#ifdef Z
    }
#endif

    //InitSpecificTime (&t_Comm, "MPI Communications");
    commflags = COLUMNDENSITYU;
    commflags += COLUMNDENSITYD;
#ifdef TAUCOLUMN
    commflags += TAUCOLUMNU;
    commflags += TAUCOLUMND;
#endif
    FARGO_SAFE( comm (commflags));

    //GiveSpecificTime (t_Comm);
  }
}
